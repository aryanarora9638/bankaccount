package ca.cmpt213.asn4.bank;

/**
 * The SavingsAccount class extends the BankAccount abstract class and manages all the user info and accounts
 * by actively using the already implemented methods in the super class
 * @author aryanarora
 */

public class SavingsAccount extends BankAccount {

    public boolean status = false;

    public SavingsAccount(double newBalance, double newAnnualInterestRate) {
        super(newBalance, newAnnualInterestRate);
        if(newBalance >= 25.0){
            this.status = true;
        }
    }

    public void withDraw(double newWithdraw){
        if(status){
            super.withdraw(newWithdraw);
            if(this.balance < 25){
                this.status = false;
            }
        }
        else {
            System.out.println("Low balance alert");
            System.out.println("Account inactive");
        }
    }

    public void deposit(double newDeposit){
        if(!status){
            if((super.balance + newDeposit) > 25.0){
                this.status = true;
            }
        }
        super.deposit(newDeposit);
    }

    public void monthlyProcess(){
        if(super.numWithDraw > 4) {
            this.monthlyServiceCharge += super.numWithDraw - 4;
        }
        super.monthlyProcess();

        if(super.balance < 25){
                this.status = false;
        }
    }


}

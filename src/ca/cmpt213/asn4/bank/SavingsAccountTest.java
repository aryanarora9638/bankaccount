package ca.cmpt213.asn4.bank;

import junit.framework.TestCase;

/**
 * The SavingsAccount test class tests all the SavingsAccount class with 100% code coverage
 * from both BankAccount and SavingsAccount class
 * @author aryanarora
 */

public class SavingsAccountTest extends TestCase {

    SavingsAccount savingsAccount;

    public void setUp(){
        savingsAccount = new SavingsAccount(70,70);
    }

    public void testIllegalConstructor(){
        try{
            savingsAccount = new SavingsAccount(-70.0,-70.0);
            fail();
        }
        catch (IllegalArgumentException e){
            assertTrue(true);
        }
    }

    public void testWithDraw() {
        //valid withdraw
        savingsAccount.withdraw(20.0);
        assertEquals(50.0, savingsAccount.balance);
        assertEquals(1,savingsAccount.numWithDraw);

        //invalid withdraw
        try {
            savingsAccount.withdraw(-70.0);
            fail();
        }
        catch (IllegalArgumentException e){
            assertTrue(true);
        }
    }

    public void testWithdrawWithActiveStatus(){
        savingsAccount.balance = 70;
        savingsAccount.status = true;
        savingsAccount.withDraw(10);
        assertEquals(60.0, savingsAccount.balance);

        savingsAccount.balance = 30;
        savingsAccount.withDraw(10);
        assertFalse(savingsAccount.status);
    }

    public void testWithDrawWithInActiveAccount(){
        savingsAccount.status = false;
        savingsAccount.withDraw(70.0);
        assertEquals(70.0, savingsAccount.balance);
    }

    public void testDeposit() {
        //valid deposit
        savingsAccount.deposit(0);
        assertEquals(70.0,savingsAccount.balance);
        assertEquals(1, savingsAccount.numDeposits);

        //invalid deposit
        try{
            savingsAccount.deposit(-70.0);
            fail();
        }
        catch (IllegalArgumentException e){
            assertTrue(true);
        }
    }

    public void testWithDepositInActiveAccount(){
        savingsAccount.status = false;
        savingsAccount.deposit(70.0);
        assertEquals(140.0, savingsAccount.balance);
    }

    public void testMonthlyProcess() {
        double annualInterest = savingsAccount.annualInterestRate;
        double monthlyInterestRate = annualInterest/12;
        double monthlyInterest = savingsAccount.balance * monthlyInterestRate;
        double newBalance = savingsAccount.balance + monthlyInterest;
        int numWithDraw = 7;
        double monthlyServiceCharge = numWithDraw - 4;
        newBalance = newBalance - monthlyServiceCharge;


        savingsAccount.numWithDraw = 7;
        savingsAccount.monthlyProcess();
        assertEquals(0, savingsAccount.numDeposits);
        savingsAccount.numWithDraw = 0;
        assertEquals(0, savingsAccount.numWithDraw);
        assertEquals(0.0, savingsAccount.monthlyServiceCharge);
    }

    public void testLessBalanceInactive(){
        savingsAccount.balance = 10;
        savingsAccount.numWithDraw = 7;
        savingsAccount.annualInterestRate = 0.0;
        savingsAccount.monthlyProcess();
        assertFalse(savingsAccount.status);
    }

    public void testMonthlyProcessWithNumDepositExceeded(){
        savingsAccount.numWithDraw = 7;
        savingsAccount.monthlyProcess();
        System.out.println(savingsAccount.monthlyServiceCharge);
        assertEquals(0.0, savingsAccount.monthlyServiceCharge);
    }
}
package ca.cmpt213.asn4.bank;

/**
 * The Bank account class is an abstract class that holds the balance, annual interest rate, monthly charges
 * number of total deposit and withdraws. It calculates the and updates the balance based on the several fees and charges
 * @author aryanarora
 */


public abstract class BankAccount {
    public double balance;
    public int numDeposits;
    public int numWithDraw;
    public double annualInterestRate;
    public double monthlyServiceCharge;


    public BankAccount(double newBalance, double newAnnualInterestRate){
        if(newBalance < 0.0 || newAnnualInterestRate < 0.0){
            throw new IllegalArgumentException();
        }
        else {
            this.balance = newBalance;
            this.annualInterestRate = newAnnualInterestRate;
            this.numDeposits = 0;
            this.numWithDraw = 0;
        }

    }

    public void deposit(double newDeposit){
        if(newDeposit < 0.0){
            throw new IllegalArgumentException();
        }
        else {
            this.balance += newDeposit;
            this.numDeposits ++;
        }
    }

    public void withdraw(double newWithdraw){
        if((newWithdraw < 0.0) || (this.balance < newWithdraw)){
            throw new IllegalArgumentException();
        }
        else {
            this.balance -= newWithdraw;
            this.numWithDraw ++;
        }
    }

    public void calcInterest(){
        double monthlyInterestRate = (this.annualInterestRate/12);
        double monthlyInterest = this.balance * monthlyInterestRate;
        this.balance += monthlyInterest;

    }

    public void monthlyProcess(){
        this.balance -= this.monthlyServiceCharge;
        this.calcInterest();

        this.numWithDraw = 0;
        this.numDeposits = 0;
        this.monthlyServiceCharge = 0;
    }
}

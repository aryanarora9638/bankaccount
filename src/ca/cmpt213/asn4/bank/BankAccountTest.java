package ca.cmpt213.asn4.bank;

import junit.framework.TestCase;

/**
 * The Bankaccount test class tests all the Bankaccount class
 * @author aryanarora
 */

public class BankAccountTest extends TestCase {

    BankAccount bankAccount;

    @Override
    public void setUp(){
        bankAccount = new BankAccount(70,70) {
            @Override
            public void deposit(double newDeposit) {
                super.deposit(newDeposit);
            }
        };
    }

    public void testIllegalConstructor(){
        try{
            bankAccount = new BankAccount(-70.0,-70.0){
                @Override
                public void deposit(double newDeposit) {
                    super.deposit(newDeposit);
                }
            };
            fail();
        }
        catch (IllegalArgumentException e){
            assertTrue(true);
        }
    }

    public void testDeposit() {
        //valid deposit
        bankAccount.deposit(0);
        assertEquals(70.0,bankAccount.balance);
        assertEquals(1, bankAccount.numDeposits);

        //invalid deposit
        try{
            bankAccount.deposit(-70.0);
            fail();
        }
        catch (IllegalArgumentException e){
            assertTrue(true);
        }

    }

    public void testWithdraw() {
        //valid withdraw
        bankAccount.withdraw(20);
        assertEquals(50.0, bankAccount.balance);
        assertEquals(1,bankAccount.numWithDraw);

        //invalid withdraw
        try {
            bankAccount.withdraw(-70.0);
            fail();
        }
        catch (IllegalArgumentException e){
            assertTrue(true);
        }
    }

    public void testCalcInterest() {
        double annualInterest = bankAccount.annualInterestRate;
        double monthlyInterestRate = annualInterest/12;
        double monthlyInterest = bankAccount.balance * monthlyInterestRate;
        double newBalance = bankAccount.balance + monthlyInterest;

        assertEquals(70.0, bankAccount.annualInterestRate);
        bankAccount.calcInterest();
        assertEquals(newBalance, bankAccount.balance);
    }

    public void testMonthlyProcess() {
        double annualInterest = bankAccount.annualInterestRate;
        double monthlyInterestRate = annualInterest/12;
        double monthlyInterest = bankAccount.balance * monthlyInterestRate;
        double newBalance = bankAccount.balance + monthlyInterest;
        newBalance = newBalance - bankAccount.monthlyServiceCharge;

        bankAccount.monthlyProcess();
        assertEquals(newBalance, bankAccount.balance);
        assertEquals(0, bankAccount.numDeposits);
        assertEquals(0, bankAccount.numWithDraw);
        assertEquals(0.0, bankAccount.monthlyServiceCharge);
    }
}